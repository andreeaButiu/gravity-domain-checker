<?php
	
	GFForms::include_addon_framework();
	
	class GFDomainCheckerAddOn extends GFAddOn {
		
		protected $_version = GF_DOMAIN_CHECKER_ADDON_VERSION;
		protected $_min_gravityforms_version = '1.9';
		protected $_slug = 'domaincheckeraddon';
		protected $_path = 'domaincheckeraddon/domaincheckeraddon.php';
		protected $_full_path = __FILE__;
		protected $_title = 'Gravity Forms Domain Checker Add-On';
		protected $_short_title = 'Domain Checker Add-On';
		
		/**
			* @var object $_instance If available, contains an instance of this class.
		*/
		private static $_instance = null;
		
		/**
			* Returns an instance of this class, and stores it in the $_instance property.
			*
			* @return object $_instance An instance of this class.
		*/
		public static function get_instance() {
			if ( self::$_instance == null ) {
				self::$_instance = new self();
			}
			
			return self::$_instance;
		}
		
		/**
			* Include the field early so it is available when entry exports are being performed.
		*/
		public function pre_init() {
			parent::pre_init();
			
			if ( $this->is_gravityforms_supported() && class_exists( 'GF_Field' ) ) {
				require_once( 'includes/class-domain-checker-field.php' );
			}
			
			register_setting( 'gf_domain_checker_options', 'tlds' );
		}
		public function init_frontend() {
			parent::init_frontend();
			// add tasks or filters here that you want to perform only in the front end
			wp_localize_script( 'my_script_js', 'wdc_ajax', array(
			'ajaxurl'       => admin_url( 'admin-ajax.php' ),
			'wdc_nonce'     => wp_create_nonce( 'wdc_nonce' ))
			);
		}
		public function init_admin() {
			parent::init_admin();
			
			//add_filter( 'gform_tooltips', array( $this, 'tooltips' ) );
			//add_action( 'gform_field_appearance_settings', array( $this, 'field_appearance_settings' ), 10, 2 );
			
		}
		
		// # SCRIPTS & STYLES -----------------------------------------------------------------------------------------------
		
		/**
			* Include my_script.js when the form contains a 'simple' type field.
			*
			* @return array
		*/
		public function scripts() {
			$scripts = array(
			array(
			'handle'  => 'my_script_js',
			'src'     => $this->get_base_url() . '/js/my_script.js',
			'version' => $this->_version,
			'deps'    => array( 'jquery' ),
			'enqueue' => array(
			array( 'field_types' => array( 'domainchecker' ) ),
			),
			'callback'  => array( $this, 'localize_scripts' ),
			),
			
			);
			
			return array_merge( parent::scripts(), $scripts );
		}
		
		public function localize_scripts() {
			
			$settings_array = array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'wdc_nonce' => wp_create_nonce( 'wdc_nonce' )
			);
			wp_localize_script( 'my_script_js', 'wdc_ajax', $settings_array );
		} // END localize_scripts
		
		/**
			* Include my_styles.css when the form contains a 'simple' type field.
			*
			* @return array
		*/
		public function styles() {
			$styles = array(
			array(
			'handle'  => 'my_styles_css',
			'src'     => $this->get_base_url() . '/css/my_styles.css',
			'version' => $this->_version,
			'enqueue' => array(
			array( 'field_types' => array( 'domainchecker' ) )
			)
			)
			);
			
			return array_merge( parent::styles(), $styles );
		}
		
		// Add the text in the plugin settings to the bottom of the form if enabled for this form
		function form_submit_button($button, $form){
			$settings = $this->get_form_settings($form);
			if(isset($settings["enabled"]) && true == $settings["enabled"]){
				$text = $this->get_plugin_setting("mytextbox");
				$button = "<div>{$text}</div>" . $button;
			}
			return $button;
		}
		
		// # ADMIN FUNCTIONS -----------------------------------------------------------------------------------------------
		/**
			* Creates a custom page for this add-on.
		*/
		
		public function plugin_page(){
		?>
		<div class="wrap">
			<h1>Available TLDs</h1>
			
			<form method="post" action="options.php">
				<?php settings_fields( 'gf_domain_checker_options' ); ?>
				<?php do_settings_sections( 'gf_domain_checker_options' ); 
					
					$current_tlds = get_option('tlds');
					$current_tlds_array = explode(', ', rtrim($current_tlds, ','));
					
				?>
				<style>
				ul.form-table li {
					display: inline-block;
					width: 12%;
					padding: 0;
					margin: 0;
				}
				@media screen and (max-width: 1200px){
					ul.form-table li {
						width: 25%;
					}
				}
				</style>
				<ul class="form-table">
					<?php
						$availableDomainString = '.com, .net, .org, .co.uk, .io, .computer, .ac, .academy, .actor, .ae, .aero, .af, .ag, 
						.agency, .ai, .am, .archi, .arpa, .as, .asia, .associates, .at, .au, .aw, .ax, .az, .bar, 
						.bargains, .bayern, .be, .berlin, .bg, .bi, .bike, .biz, .bj, .blackfriday, .bn, .boutique, .build, 
						.builders, .bw, .by, .ca, .cab, .camera, .camp, .capital, .cards, .careers, .cat, .catering, 
						.cc, .center, .ceo, .cf, .ch, .cheap, .christmas, .ci, .cl, .cleaning, .clothing, .club, 
						.cn, .co, .codes, .coffee, .college, .cologne, .community, .company, .construction, 
						.contractors, .cooking, .cool, .coop, .country, .cruises, .cx, .cz, .dating, .de, 
						.democrat, .desi, .diamonds, .directory, .dk, .dm, .domains, .dz, .ec, .edu, .education,
						.ee, .email, .engineering, .enterprises, .equipment, .es, .estate, .eu, .eus, .events,
						.expert, .exposed, .farm, .feedback, .fi, .fish, .fishing, .flights, .florist, .fo, 
						.foo, .foundation, .fr, .frogans, .futbol, .ga, .gal, .gd, .gg, .gi, .gift, .gl, .glass,
						.gop, .gov, .graphics, .gripe, .gs, .guitars, .guru, .gy, .haus, .hk, .hn, .holiday, 
						.horse, .house, .hr, .ht, .hu, .id, .ie, .il, .im, .immobilien, .in, .industries, 
						.institute, .int, .international, .iq, .ir, .is, .it, .je, .jobs, .jp, .kaufen, .ke, 
						.kg, .ki, .kitchen, .kiwi, .koeln, .kr, .kz, .la, .land, .lease, .li, .lighting, .limo, 
						.link, .london, .lt, .lu, .luxury, .lv, .ly, .ma, .management, .mango, .marketing, .md,
						.me, .media, .menu, .mg, .miami, .mk, .ml, .mn, .mo, .mobi, .moda, .monash, .mp, .ms,
						.mu, .museum, .mx, .my, .na, .name, .nc, .nf, .ng, .ninja, .nl, .no, .nu, .nz, .om, 
						.onl, .paris, .partners, .parts, .pe, .pf, .photo, .photography, .photos, .pics, 
						.pictures, .pl, .plumbing, .pm, .post, .pr, .pro, .productions, .properties, .pt, 
						.pub, .pw, .qa, .quebec, .re, .recipes, .reisen, .rentals, .repair, .report, .rest, 
						.reviews, .rich, .ro, .rocks, .rodeo, .rs, .ru, .ruhr, .sa, .saarland, .sb, .sc, .se,
						.services, .sexy, .sg, .sh, .shoes, .si, .singles, .sk, .sm, .sn, .so, .social, .solar, 
						.solutions, .soy, .st, .su, .supplies, .supply, .support, .sx, .sy, .systems, .tattoo, 
						.tc, .technology, .tel, .tf, .th, .tienda, .tips, .tk, .tl, .tm, .tn, .to, .today, 
						.tools, .town, .toys, .tr, .training, .travel, .tv, .tw, .tz, .ua, .ug, .uk, .university, 
						.us, .uy, .black, .blue, .info, .kim, .pink, .red, .shiksha, .uz, .vacations, .vc, .ve,
						.vegas, .ventures, .vg, .viajes, .villas, .vision, .vodka, .voting, .voyage, .vu, .wang,
						.watch, .wed, .wf, .wien, .wiki, .works, .ws, .xxx, .xyz, .yt, .za, .zm, .zone';
						$availableDomains = explode(', ', $availableDomainString);
						
						foreach ($availableDomains as &$domain) {
							$domainID = str_replace('.','',$domain);	
						?>
						<li><input type="checkbox" class="tld_select" <?php echo in_array($domainID,$current_tlds_array) ? 'checked' : ''; ?> name="<?php echo $domainID; ?>"> <label for="<?php echo $domainID; ?>"><?php echo $domain; ?></label></li>
						<?php 
						} ?>
				</ul>
				<input type="hidden" id="tlds" name="tlds" value="testing" />
				<?php submit_button(); ?>
				
			</form>
		</div>
		<script type="text/javascript">
			
			jQuery(document).ready( function () { 
				jQuery('.tld_select').change(function() {
					
					var tlds_object = jQuery('.tld_select:checkbox:checked').serializeArray();
					console.log(tlds_object);
					jQuery('#tlds').val('');
					jQuery.each( tlds_object, function( i, tld ) {
						current_val = jQuery('#tlds').val();
						
						jQuery('#tlds').val(current_val+tld.name+', ');
					});
				});	
				
			});
			
		</script>
		<?php 
		}
		
		
	}			