<?php
/*
	Plugin Name: Gravity Forms Domain Checker
	Plugin URI: http://www.gravityforms.com
	Description: A simple add-on to check for domain availability
	Version: 0.1
	
	-----------------------------------------------------------------------
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

define( 'GF_DOMAIN_CHECKER', '2.1' );

add_action( 'gform_loaded', array( 'GF_Domain_Checker_AddOn_Bootstrap', 'load' ), 5 );

class GF_Domain_Checker_AddOn_Bootstrap {
	
	public static function load() {
		
		if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
			return;
		}
		
		require_once( 'class-gfdomaincheckeraddon.php' );
		
		GFAddOn::register( 'GFDomainCheckerAddOn' );
	}
	
}

function gf_simple_addon() {
	return GFDomainCheckerAddOn::get_instance();
}

function wdc_display_func(){
	check_ajax_referer( 'wdc_nonce', 'security' );
	if(isset($_POST['name']) && isset($_POST['tld']))
	{
		$domainname = str_replace(array('www.', 'http://'), NULL, $_POST['name']);
		$domain = $domainname.'.'.$_POST['tld'];
		if(strlen($domainname) > 0)
		{
			include ('includes/DomainAvailability.php');  
			$Domains = new DomainAvailability();
			$available = $Domains->is_available($domain);
			$custom_found_result_text = __('The domain you selected is available!', 'wdc');
			$custom_not_found_result_text = __('The domain you selected is already taken!', 'wdc');
			
			if ($available == '1') {
				$result = array('status'=>1,'domain'=>$domain, 'text'=> '<p class="available">'.$custom_found_result_text.'</p>');
				echo json_encode($result);
				} elseif ($available == '0') {
				$result = array('status'=>0,'domain'=>$domain, 'text'=> '<p class="not-available">'.$custom_not_found_result_text.'</p>');
				echo json_encode($result);
				}elseif ($available == '2'){
				$result = array('status'=>0,'domain'=>$domain, 'text'=> '<p class="not-available">WHOIS server not found for that TLD</p>');
				echo json_encode($result);
			}
			
		}
		else
		{
			$result = array('status'=>0,'domain'=>$domain, 'text'=> '<p class="not-filledin">Please enter the domain name</p>');
			echo json_encode($result);
		}
	} 
	die();
}
add_action('wp_ajax_wdc_display_func','wdc_display_func');
add_action('wp_ajax_nopriv_wdc_display_func','wdc_display_func');