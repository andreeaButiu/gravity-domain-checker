// placeholder for javascript
jQuery.noConflict();
(function( $ ) {
	$(function() {
		$('.select_tld').on('change', function() {
		  $(this).closest('.ginput_container').find('.check_domain_availability').prop("disabled", false);
		});
		// More code using $ as alias to jQuery
		
		$('.check_domain_availability').click(function(event) {
			event.preventDefault();
			var ginput_container = $(this).closest('.ginput_container');
			var DomainName = ginput_container.find('input[type="text"]').val();
		
			var DomainTLD = ginput_container.find('select').find(":selected").text();
			ginput_container.find('input[type="hidden"]').val(DomainName+'.'+DomainTLD);
			
			$.ajax({
				url: wdc_ajax.ajax_url,
				data: {
					action: "wdc_display_func",
					name: DomainName,
					tld: DomainTLD,
					security: wdc_ajax.wdc_nonce
				},
				error: function() {
					ginput_container.find('.gfdc_info').html('<p>An error has occurred</p>');
				},
				dataType: 'json',
				success: function(data) {
					ginput_container.find('.gfdc_info').html(data.text);
				},
				type: 'POST'
			});
		});
	});
})(jQuery);